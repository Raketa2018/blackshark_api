"""
Routes and data objects for querying/updating terrain information
"""
import logging
from typing import List

from fastapi import APIRouter
from pydantic import BaseModel

from libs.terrain_utils import tile_statistics, TileStatistics
from models.database import DatabaseSession
from models.terrain import Terrain, TerrainTile

_logger = logging.getLogger(__name__)


class TerrainItem(BaseModel):
    """
    Response model for Terrain
    Contains some additional values that are calculated on the fly
    """

    id: int
    name: str
    tile_count: int = 0


def terrain_router() -> APIRouter:
    """
    Routes for terrain related queries
    """
    router = APIRouter()

    @router.get("/", response_model=List[TerrainItem])
    def _route_terrain_collection() -> List[TerrainItem]:
        with DatabaseSession() as session:
            terrains = session.query(Terrain)
            result = []
            for terrain in terrains:
                tiles = session.query(TerrainTile).filter_by(terrain=terrain).all()
                result.append(TerrainItem(id=terrain.id, name=terrain.name, tile_count=len(tiles)))
            return result

    @router.get("/tile/{tile_id}/stats")
    def _route_pass(tile_id: int) -> TileStatistics:
        return tile_statistics(tile_id)

    return router
