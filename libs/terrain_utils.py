"""
Basic utilities for handling terrain tiles
"""
from pathlib import Path
from typing import List

import numpy as np
from pydantic import BaseModel

from models.database import DatabaseSession
from models.terrain import TerrainTile

tile_base_path = Path(__file__).parent / ".." / "data"


class TileStatistics(BaseModel):
    """
    Response model for tile statistics, also used as FastAPI response
    """

    pos_x: int
    pos_y: int
    width: int
    height: int
    average: float
    max_per_row: List[float]
    min_per_col: List[float]


def tile_statistics(tile_id: int) -> TileStatistics:
    """
    Calculates basic statistics on TerrainTile
    """
    with DatabaseSession() as session:
        tile = session.query(TerrainTile).get(tile_id)

    data = np.load(tile_base_path / tile.path)
    shape = np.shape(data)
    return TileStatistics(
        pos_x=tile.pos_x,
        pos_y=tile.pos_y,
        width=shape[0],
        height=shape[1],
        average=np.average(data),
        max_per_row=data.max(axis=1).tolist(),
        min_per_col=data.min(axis=0).tolist(),
    )
